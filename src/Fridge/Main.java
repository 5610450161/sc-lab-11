package Fridge;

public class Main extends Refrigerator{
	public static void main(String[] args){
		Refrigerator fridge = new Refrigerator();
		
		fridge.put("apple");
		fridge.put("banana");
		fridge.put("watermalon");
		fridge.put("orange");
		
		fridge.takeout("apple");
	}
}
