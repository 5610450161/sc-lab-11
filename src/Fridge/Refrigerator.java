package Fridge;

import java.util.ArrayList;

public class Refrigerator {
	private int size = 3;
	private int cursize = 0;
	ArrayList<String> thing = new ArrayList<String>();
	
	public void put(String stuff){
		if(cursize != size){
			thing.add(stuff);
			System.out.println("Add : "+stuff);
			cursize += 1;
			
			if(cursize == size){
				System.out.println("Fridge Full");
			}
		}else{
			System.err.println("Error : Fridge Full");
		}
	}
	
	public void takeout(String stuff){
		for(int i = 0; i != thing.size(); i ++){
			if(thing.get(i) == stuff){
				System.out.println("Take : "+thing.get(i));
				thing.remove(i);
			}
		}
	}
}
