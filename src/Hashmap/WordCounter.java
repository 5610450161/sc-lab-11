package Hashmap;
import java.util.HashMap;


public class WordCounter {
	HashMap<String, Integer> wordCount ;
	private String massage;
	
	public WordCounter(String message){
		this.massage = massage;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void count(){
		int value = 1;
		String[] s = massage.split(" ");
		
		for(int i = 0; i < s.length; i++){
			if(!wordCount.containsKey(s[i])){
				wordCount.put(s[i], 1);
				
			}
			else{
				
				wordCount.replace(s[i], wordCount.get(s[i]), wordCount.get(s[i])+1);
			}
			
		}
		
	}
	
	public int hasWord(String word){
		return wordCount.get(word) ;
	}
}
